﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Animation
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            #region 1st
            //DoubleAnimation animation = new DoubleAnimation();
            //animation.From = rectangle.Height; // берем высоту действитульную
            //animation.To = rectangle.Height + 150; // меняем высоту
            //animation.Duration = new Duration(TimeSpan.FromSeconds(5)); // время анимации. чем больше время, тем медленнее анимация
            //animation.RepeatBehavior = RepeatBehavior.Forever; // будет ли продолжаться анимация

            //animation.AutoReverse = true; // плавное опускание высоты чтобы повторялось плавно

            //Storyboard storyboard = new Storyboard();
            //storyboard.Children.Add(animation);
            //Storyboard.SetTargetName(animation, rectangle.Name); // привязываем анимацию с элементом управления
            //Storyboard.SetTargetProperty(animation, new PropertyPath(HeightProperty)); // связываем анимацию с конкретными проперти
            //storyboard.Begin(this); // владельцем анимации является окно
            #endregion

            DoubleAnimation animation = new DoubleAnimation();

            animation.From = rectangle.Width;
            animation.To = rectangle.Width + 150;
            animation.Duration = new Duration(TimeSpan.FromSeconds(10));
            animation.FillBehavior = FillBehavior.HoldEnd;
            animation.Completed += Done;

            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(animation);
            Storyboard.SetTargetName(animation, rectangle.Name); // привязываем анимацию с элементом управления
            Storyboard.SetTargetProperty(animation, new PropertyPath(WidthProperty)); // связываем анимацию с конкретными проперти
            storyboard.Begin(this); // владельцем анимации является окно

            
        }
        private void Done(object sender, EventArgs e)
        {
            MessageBox.Show("Done!");
        }
    }

}
